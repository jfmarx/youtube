-- custom SQL for tableau server extract (dummy data) JUST UDIGS
select 
	day_idnt
	, day_dt
	, wk_of_fyr
	, wk_idnt
	, wk_end_dt
	, event_type
	, event_day
	, price_type
	, ty_ly_ind
	, channel
	, banner
	, country
	, style_num
	, style_desc
	, vpn
	, supp_color
	, style_group_idnt
	, division_group
	, division
	, subdivision
	, department
	, "class"
	, subclass
	, supplier
	, brand
	, product_type
	, parent_group
	, holiday_theme
	, price_band_one
	, price_band_two
	, max(rp_ind                         )  as rp_ind
	, max(npg_ind                        )  as npg_ind
	, max(dropship_ind                   )  as dropship_ind
	, max(price_match_ind                )  as price_match_ind
	, max(gift_ind                       )  as gift_ind
	, max(bipoc_ind                      )  as bipoc_ind
	, sum(sales_units                    )	as sales_units
	, sum(sales_dollars                  )	as sales_dollars
	, sum(return_units                   )	as return_units
	, sum(return_dollars                 )	as return_dollars
	, sum(ntn                            )	as ntn
	, sum(demand_units                   )	as demand_units
	, sum(demand_dollars                 )	as demand_dollars
	, sum(shipped_units                  )	as shipped_units
	, sum(shipped_dollars                )	as shipped_dollars
	, sum(eoh_units                      )	as eoh_units
	, sum(eoh_dollars                    )	as eoh_dollars
	, sum(nonsellable_units              )	as nonsellable_units
	, sum(receipt_units                  )	as receipt_units
	, sum(receipt_dollars                )	as receipt_dollars
	, avg(sales_aur                      )	as sales_aur
	, avg(demand_aur                     )	as demand_aur
	, avg(eoh_aur                        )	as eoh_aur
	, sum(dtc_units                      )	as dtc_units
	, sum(dtc_dollars                    )	as dtc_dollars
	, sum(store_fulfill_units            )	as store_fulfill_units
	, sum(store_fulfill_dollars          )	as store_fulfill_dollars
	, sum(dropship_units                 )	as dropship_units
	, sum(dropship_dollars               )	as dropship_dollars
	, sum(demand_dropship_units          )	as demand_dropship_units
	, sum(demand_dropship_dollars        )	as demand_dropship_dollars
	, sum(receipt_dropship_units         )	as receipt_dropship_units
	, sum(receipt_dropship_dollars       )	as receipt_dropship_dollars
	, sum(on_order_units                 )	as on_order_units
	, sum(on_order_retail_dollars        )	as on_order_retail_dollars
	, sum(on_order_4wk_units             )	as on_order_4wk_units
	, sum(on_order_4wk_retail_dollars    )	as on_order_4wk_retail_dollars
	, sum(backorder_units                )	as backorder_units
	, sum(backorder_dollars              )	as backorder_dollars
	, sum(product_views                  )	as product_views
	, sum(cart_adds                      )	as cart_adds
	, sum(order_units                    )	as order_units
	, sum(instock_views                  )	as instock_views
	, sum(demand                         )	as demand
	, sum(wishlist_adds                  )	as wishlist_adds
	, max(last_receipt_date              )	as last_receipt_date
	, max(udig_main                      )	as udig_main
from 
	prd_ma_digitalbi.holiday_cyber_test_data 
group by 	day_idnt
	, day_dt
	, wk_of_fyr
	, wk_idnt
	, wk_end_dt
	, event_type
	, event_day
	, price_type
	, ty_ly_ind
	, channel
	, banner
	, country
	, style_num
	, style_desc
	, vpn
	, supp_color
	, style_group_idnt
	, division_group
	, division
	, subdivision
	, department
	, "class"
	, subclass
	, supplier
	, brand
	, product_type
	, parent_group
	, holiday_theme
	, price_band_one
	, price_band_two
having max(udig_main) is not null 
;

--aggregated to subclass-supplier
select 
	day_idnt
	, day_dt
	, wk_of_fyr
	, wk_idnt
	, wk_end_dt
	, event_type
	, event_day
	, price_type
	, ty_ly_ind
	, channel
	, banner
	, country
	, division_group
	, division
	, subdivision
	, department
	, "class"
	, subclass
	, supplier
	, brand
	, product_type
	, parent_group
	, holiday_theme
	, price_band_one
	, price_band_two
	, rp_ind         
	, npg_ind                        
	, dropship_ind       
	, price_match_ind               
	, gift_ind                       
	, bipoc_ind                     
	, sum(sales_units                    )	as sales_units
	, sum(sales_dollars                  )	as sales_dollars
	, sum(return_units                   )	as return_units
	, sum(return_dollars                 )	as return_dollars
	, sum(ntn                            )	as ntn
	, sum(demand_units                   )	as demand_units
	, sum(demand_dollars                 )	as demand_dollars
	, sum(shipped_units                  )	as shipped_units
	, sum(shipped_dollars                )	as shipped_dollars
	, sum(eoh_units                      )	as eoh_units
	, sum(eoh_dollars                    )	as eoh_dollars
	, sum(nonsellable_units              )	as nonsellable_units
	, sum(receipt_units                  )	as receipt_units
	, sum(receipt_dollars                )	as receipt_dollars
	, avg(sales_aur                      )	as sales_aur
	, avg(demand_aur                     )	as demand_aur
	, avg(eoh_aur                        )	as eoh_aur
	, sum(dtc_units                      )	as dtc_units
	, sum(dtc_dollars                    )	as dtc_dollars
	, sum(store_fulfill_units            )	as store_fulfill_units
	, sum(store_fulfill_dollars          )	as store_fulfill_dollars
	, sum(dropship_units                 )	as dropship_units
	, sum(dropship_dollars               )	as dropship_dollars
	, sum(demand_dropship_units          )	as demand_dropship_units
	, sum(demand_dropship_dollars        )	as demand_dropship_dollars
	, sum(receipt_dropship_units         )	as receipt_dropship_units
	, sum(receipt_dropship_dollars       )	as receipt_dropship_dollars
	, sum(on_order_units                 )	as on_order_units
	, sum(on_order_retail_dollars        )	as on_order_retail_dollars
	, sum(on_order_4wk_units             )	as on_order_4wk_units
	, sum(on_order_4wk_retail_dollars    )	as on_order_4wk_retail_dollars
	, sum(backorder_units                )	as backorder_units
	, sum(backorder_dollars              )	as backorder_dollars
	, sum(product_views                  )	as product_views
	, sum(cart_adds                      )	as cart_adds
	, sum(order_units                    )	as order_units
	, sum(instock_views                  )	as instock_views
	, sum(demand                         )	as demand
	, sum(wishlist_adds                  )	as wishlist_adds
	, max(last_receipt_date              )	as last_receipt_date
	, max(udig_main                      )	as udig_main
from 
	prd_ma_digitalbi.holiday_cyber_test_data 
group by 	day_idnt
	, day_dt
	, wk_of_fyr
	, wk_idnt
	, wk_end_dt
	, event_type
	, event_day
	, price_type
	, ty_ly_ind
	, channel
	, banner
	, country
	, division_group
	, division
	, subdivision
	, department
	, "class"
	, subclass
	, supplier
	, brand
	, product_type
	, parent_group
	, holiday_theme
	, price_band_one
	, price_band_two
	, rp_ind         
	, npg_ind                        
	, dropship_ind       
	, price_match_ind               
	, gift_ind                       
	, bipoc_ind