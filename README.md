# YouTube Trending Videos
- <a href="https://public.tableau.com/profile/jessica.marx#!/vizhome/trending_youtube/YouTubeTrendingVideos" target="_blank">Tableau workbook</a> 
- <a href="https://jfmarx.gitlab.io/youtube/data-explore-clean.nb.html" target="_blank">Data manipulation (R notebook)</a> 

<br>

## Dataset
- _Countries:_ United States and Great Britain
- _Dates:_ September 13-30, 2017

<br>

## Workbook Overview
### Daily Snapshot
_When you want an at-a-glance breakdown._
- Reference daily KPIs at country-wide level and how they’ve changed since the previous day. 
- Compare daily total views by video category between countries. 
- See which tags stand out on that particular day. 

### Leaderboard
_When you want to drill down to the details._
- Apply a variety of filters to see where videos rank by views and engagement. 
- Hover over a selection to see a video’s three most-liked comments. 

### Trends over Time 
_When you want a time series perspective._
- Choose a date span and find the patterns in views and engagement by country. 
- Learn which channels have been dominating the Trending charts during that time. 

<br>

## Data Dictionary
| Metric / Term 	| Definition 	|
|-	|-	|
| Accelerator 	| Filter distinguishing   videos that have a day over day percentage increase significantly greater   than those of their peers (country and date); upper outliers. This filter only   applies to videos that have been trending for more than 1 day. 	|
| Category   Name 	| Description of the content generally applicable to all or most videos   under a "category id" number. This was derived using tf-idf scores   of the words in video titles.  	|
| Comments per View 	| Proportion of comments per   view (also seen as "Comments / View").  	|
| Daily   Dislikes 	| Total dislikes (cumulative) as of selected date (range).  	|
| Daily Likes 	| Total likes (cumulative) as   of selected date (range).  	|
| Daily   Views 	| Total views (cumulative) as of selected date (range).  	|
| Dominating Channels 	| Count of total number of   videos per channel that have trended during the selected time period and   sorted in descending order by that number. 	|
| Likes   per View 	| Proportion of likes per view (also seen as "Likes /   View").  	|
| Likes to Dislikes Ratio 	| Ratio of current number of   likes to dislikes; also seen as "Likes : Dislikes." 	|
| Marathoner 	| Filter distinguishing videos that have been trending significantly   longer (consecutive days) than their peers (country and date); upper outliers.  	|
| Mean Days Trending 	| Average number of days   spent on the trending list.  	|
| Pareto   Proportion 	| A way to measure the 80/20 rule; the proportion/percentage of videos   that account for 80% of daily views.  	|
| Proportion New 	| Percentage of videos that   are on day 1 of consecutive views.  	|
| Standout   Tags 	| Top 20 tags partitioned by day and country as ranked by the tf-idf   score of the tag (see TFIDF for more details).  	|
| TFIDF 	| Term frequency–inverse   document frequency; reflects how important a word or term is to a document in   a collection. The tf–idf value increases proportionally to the number of   times a word or term appears in the document and is offset by the number of   documents (or videos) in the group that contain the word; this helps to   adjust for the fact that some words appear more frequently in general   (vlogging, comedy, etc.).  	|
| Top   Comments 	| Ranked by number of likes per comment (i.e. comment with the most likes   is top).  	|
| View Count Decile 	| Deciles by daily views;   decile 1 represents the top 10% of daily views; decile 2 = the top   10-20%...and so on.  	|

<br>

## Next Steps:
### Ideas for v2
- Incorporate population data to scale metrics per capita. 
- Use NLP to:
    - Flag videos with offensive comments and/or content. 
    - Segment video content and top comments by sentiment. For example, within the Beauty category: a beauty influencer announcing a new product launch vs. making an apology. 
- Provide insight into performance by day of the week.
- If possible, deliver metrics a more granular level than by day -- by hour, for example.  

